# Reset APT Puppet Module Changelog

## 2021-03-15 Release 6.0.0

- Updated for Puppet 6 compliance.

## 2015-03-04 Release 1.0.4

- Migrated from github to bitbucket
- Changed ownership of puppetforge account
- Changed to landcareresearch/check_run

## 2015-01-16 - Version 1.0.3

- Attempts to comply with code styling.

## 2014-2-5 - Version 1.0.2

- Changed to using check_run module for handling one time only apt refresh.

## 2013-12-30 - Version 1.0.1

- This release added a test and a Change log.

## 2013-12-30 - Version 1.0.0

- Initial Release
